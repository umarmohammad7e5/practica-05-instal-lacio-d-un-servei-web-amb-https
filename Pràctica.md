
# Pràctica 05 - Instal·lació d’un servei web amb HTTPS
Umar Mohammad - 2nB ASIXc

---

### Índex
- [Pràctica 05 - Instal·lació d’un servei web amb HTTPS](#pràctica-05---installació-dun-servei-web-amb-https)
    - [Índex](#índex)
  - [1. EC2 websegura](#1-ec2-websegura)
    - [1.1. Creació de la instància EC2](#11-creació-de-la-instància-ec2)
    - [1.2. Elastic IP](#12-elastic-ip)
  - [2. DuckDNS](#2-duckdns)
    - [2.1. DNS Checker](#21-dns-checker)
  - [3. LAPP (Linux, Apache, PostgreSQL, PHP)](#3-lapp-linux-apache-postgresql-php)
    - [3.1. Apache](#31-apache)
    - [3.2. PostgreSQL](#32-postgresql)
    - [3.3. PHP](#33-php)
  - [4. Drupal](#4-drupal)
    - [4.1. Configuració Apache](#41-configuració-apache)
    - [4.2. Base de dades](#42-base-de-dades)
    - [4.3. Configuració Drupal](#43-configuració-drupal)
  - [5. HTTPS](#5-https)
  - [6. HTTP Sniffing](#6-http-sniffing)
  - [7. Referències](#7-referències)

---

## 1. EC2 websegura

### 1.1. Creació de la instància EC2

Primerament, crearem una instància EC2 Ubuntu Server, amb el security group que permeti el tràfic HTTP i HTTPS, jo aprofitaré els que ja tinc creats per anteriors pràctiques.

![webserguraec2deploy](https://user-images.githubusercontent.com/92973740/235736364-14eb281b-e9c6-459a-9ca2-430274f290ab.gif)


### 1.2. Elastic IP

A la màquina assignarem una IP fixa, per això crearem una Elastic IP i l'assignarem a la instància EC2.

![iplas](https://user-images.githubusercontent.com/92973740/236865904-dd1c9340-dcf9-4c9d-9f07-824dbed9c5c8.gif)

Seleccionaré l'EC2 i li assignaré la IP elàstica:

![IPLAS-ELASTIC](https://user-images.githubusercontent.com/92973740/236866175-479f88f6-2430-4f43-ad43-a4cbdde122fc.gif)

## 2. DuckDNS

Després de tenir la IP elàstica, crearem un compte a DuckDNS per a poder tenir un domini que apunti a la IP elàstica. 

Primer registrem el domini, en el meu cas he triat el domini umarmohammad.duckdns.org:

![duckdnsdomain](https://user-images.githubusercontent.com/92973740/236880942-71003a45-3730-402a-97ba-f4f050964cef.gif)

Després amb la IP elàstica l'assignem al domini:

![DUCKIP](https://user-images.githubusercontent.com/92973740/236886840-0332003c-a1f8-4b49-ad82-6a88df5aef77.gif)

Amb aquesta configuració ja podem accedir a la nostra instància EC2 amb el domini que hem creat. Si no disposéssim d'una IP elàstica, el domini no funcionaria quan es canviï la IP pública de la instància. Per evitar s'hauria de configurar un script que s'executi cada cop que la IP pública canvia, però això no cal en el nostre cas.

### 2.1. DNS Checker

Per comprovar que el domini apunta a la IP elàstica, utilitzarem la pàgina web [MXToolBox](https://mxtoolbox.com/SuperTool.aspx?action=a%3aumarmohammad.duckdns.org&run=toolpage), com es veu a la imatge següent, el domini apunta a la IP elàstica d'AWS correctament:

![image](https://user-images.githubusercontent.com/92973740/236887796-773efbee-0117-42ed-b147-231e18c9a8fd.png)


## 3. LAPP (Linux, Apache, PostgreSQL, PHP)

Com altres CMS (Content Management System), Drupal també necessita la pila LAPP (Linux, Apache, PostgreSQL, PHP) per a funcionar.

### 3.1. Apache

Per instalar Apache executarem la següent comanda:

```bash
sudo apt install apache2
```

Podem comprovar el funcionament amb l’ordre **sudo service apache2 status** que ens retorna l’estat del servei d’Apache i accedint a la IP pública de la instància amb el navegador per veure si carrega la pàgina per defecte d’Apache2:

![image](https://user-images.githubusercontent.com/92973740/236885023-47c247df-2536-4269-8fa8-db0c492a712d.png)


### 3.2. PostgreSQL

Per instal·lar PostgreSQL executarem la següent comanda:

```bash
sudo apt install postgresql
```
![image](https://user-images.githubusercontent.com/92973740/236896248-ae5520d6-db43-455b-ac9f-5045ae6f12ae.png)

### 3.3. PHP

Per instal·lar PHP executarem la següent comanda:

```bash 
sudo apt install -y php8.1-apcu php8.1-gd php8.1-mbstring php8.1-uploadprogress php8.1-xml

# Llibreria de PHP per PostgreSQL
sudo apt install -y php8.1-pgsql
```


Després, cal modificar el directori **/etc/apache2/mods-enabled/dir.conf**, perquè Apache pugui llegir arxius .php, per això cal deixar-ho de la següent forma:

```bash
<IfModule mod_dir.c>
    DirectoryIndex index.php index.html index.cgi index.pl index.xhtml index.htm
</IfModule>
```

Amb aquests passos ja tindrem PHP i les llibreries necessàries per a que Drupal funcioni.


## 4. Drupal

Per instalar cal descarregar el paquet de Drupal, per això executarem la següent comanda:

```bash
wget --content-disposition https://www.drupal.org/download-latest/tar.gz
```

Després descomprimim el paquet:

```bash
sudo tar xf drupal-10.X.X.tar.gz -C /var/www/html
```

I canviem els permisos del directori per a que Apache pugui llegir i escriure:

```bash
sudo chown -R www-data: /var/www/html
sudo chmod -R 755 /var/www/html
```

### 4.1. Configuració Apache

Cal crear una configuració del site en Apache que ho farem en **/etc/apache2/sites-available/**, aquest arxiu conté informació sobre el servidor i el site:

![image](https://user-images.githubusercontent.com/92973740/236905310-e60f0b16-7cea-4d1c-ba1e-9e0b9a123881.png)

Després cal activar el site amb la següent comanda:

```bash
sudo a2ensite drupal.conf
```

I els següents mòduls:

```bash
sudo a2enmod expires headers rewrite
```


### 4.2. Base de dades

Ara crearem la base de dades per a Drupal, per això executarem les següents comandes:

```bash
psql
postgres createuser -P drupal
postgres createdb drupal -O drupal
```

Executem les ordres i ja tindrem la base de dades creada:

![image](https://user-images.githubusercontent.com/92973740/236906456-12fe867f-daf8-44e7-b497-30de623035b2.png)

### 4.3. Configuració Drupal

Amb la base de dades i la configuració d'Apache feta, ja podem bé accedir per IP o per domini DuckDNS a la nostra instància EC2 i configurar Drupal.

Primerament configurem l'idioma:

![image](https://user-images.githubusercontent.com/92973740/236908119-f6803380-9dd5-42ac-893a-1cc0d4c62bfe.png)

Ara el tipus d'instal·lació, en el meu cas continuaré amb la instal·lació estàndard:

![image](https://user-images.githubusercontent.com/92973740/236908207-c9437c9b-09e1-4119-bb99-9b39d9f18203.png)

Ara ens dona informació sobre els requisits, com podem veure tots els requisits obligatoris estan complets i podem continuar:

![image](https://user-images.githubusercontent.com/92973740/236908304-b4622ab2-d5ef-48bd-8fa0-5a537b2098ae.png)

Ara cal configurar la base de dades, per això caldrà posar el nom de la base de dades, l'usuari i la contrasenya que hem creat anteriorment:

![image](https://user-images.githubusercontent.com/92973740/236908484-ab8f5a82-8044-41f9-929f-18912349c3a3.png)

I finalment caldrà configurar el site, en el meu cas he posat el nom del site, el correu electrònic i la contrasenya de l'usuari administrador:

![image](https://user-images.githubusercontent.com/92973740/236939352-487158e1-227f-4e59-bec5-394c1ad4d198.png)

Com es veu ja puc accedir amb IP o com es veu a la imatge següent amb el domini DuckDNS:

![image](https://user-images.githubusercontent.com/92973740/236939596-a0cdb783-f600-4c08-8a49-9cfd3ad22497.png)

## 5. HTTPS

En aquest apartat configurarem el nostre site amb HTTPS, com podem veure el nostre domini no té HTTPS actualment:

![image](https://user-images.githubusercontent.com/92973740/236940720-e55471ab-6ff0-4681-b7ca-178c1436ecdc.png)

Per a poder tenir el nostre site amb HTTPS, caldrà instal·lar un certificat SSL, per això utilitzarem Let's Encrypt, que és una autoritat de certificació que proporciona certificats SSL gratuïts.

Per a instal·lar-lo executarem les següents comandes per obtenir certbot, un script que ens ajudarà a instal·lar el certificat SSL:

```bash
sudo service apache2 stop # Cal aturar el servei d'Apache per a que certbot pugui utilitzar el port 80
sudo apt install certbot # Instal·lem certbot
sudo certbot certonly --standalone -d umarmohammad.duckdns.org # Creem el certificat SSL
```

![image](https://user-images.githubusercontent.com/92973740/236948616-e652ca67-24c3-4514-9f36-0150c890f02d.png)

Ara cal crear un nou virtual host per a que Apache pugui utilitzar el certificat SSL, per això crearem un nou arxiu de configuració en **/etc/apache2/sites-available/**:

![image](https://user-images.githubusercontent.com/92973740/236950270-0922972b-8111-46ba-8808-d4deb43b07bb.png)

Activem el site i reiniciem el servei d'Apache:

```bash
sudo a2ensite drupal-ssl.conf
sudo service apache2 restart
```

I ja tindrem el nostre site amb HTTPS en el nostre domini DuckDNS (https://umarmohammad.duckdns.org/): 

![image](https://user-images.githubusercontent.com/92973740/236950507-4a50a6c9-68ea-49e6-9d4b-a2b70d9ec614.png)

Com veiem a la imatge, el certificat SSL és vàlid i està emès per Let's Encrypt per a umarmohammad.duckdns.org i és vàlid 90 dies:

![image](https://user-images.githubusercontent.com/92973740/236950593-80b57839-9d2f-4ad6-a707-eddbba5d72d0.png)

**🗒️ NOTA:** En el security group i en el tallafoc de la instància EC2 cal obrir el port 443 perquè HTTPS funcioni correctament.

## 6. HTTP Sniffing

En el següent [vídeo de YouTube](https://www.youtube.com/watch?v=Ou5n0INOE_8) es pot veure la manca de seguretat del protocol HTTP i la seguretat del HTTPS:

[![](http://img.youtube.com/vi/Ou5n0INOE_8/0.jpg)](https://www.youtube.com/watch?v=Ou5n0INOE_8)

## 7. Referències

- [Cómo instalar Drupal en Ubuntu 20.04 LTS](https://comoinstalar.me/como-instalar-drupal-en-ubuntu-20-04-lts/)

- [How to Install Drupal with Apache2 and Let’s Encrypt Free SSL on Ubuntu 18.04](https://help.clouding.io/hc/en-us/articles/360010683519-How-to-Install-Drupal-with-Apache2-and-Let-s-Encrypt-Free-SSL-on-Ubuntu-18-04)

- [Certbot DNS DuckDNS Plugin](https://pypi.org/project/certbot-dns-duckdns/)

- [Wireshark Tutorial: Network & Passwords Sniffer](https://www.guru99.com/wireshark-passwords-sniffer.html)









